package main;

import AerialVehicles.*;
import Entities.*;
import Missions.*;

import java.util.*;

public class Solution {
    public static void main(String[] args){
        Destination homeBase1 = new Destination(new Coordinates(31.123123,32.321321), "Ramat David");
        Destination homeBase2 = new Destination(new Coordinates(31.321321,32.123123), "Palmahim");
        Destination target1 = new Destination(new Coordinates(31.001337,32.42069), "some fucking weeb's " +
                "house");
        Destination target2 = new Destination(new Coordinates(31.42069,32.001337), "the guy who invented " +
                "TikTok");
        List<String> loadout1 = Arrays.asList("Python","Python","Python","Python","Spice250","Spice250","AMRAAM",
                "AMRAAM","AMRAAM");
        List<String> loadout2 = Arrays.asList("Python","Python","AMRAAM","AMRAAM","Spice250","Spice250","Spice250",
                "Spice250","AMRAAM","AMRAAM");

        F15 baz = new F15(0, "ready", homeBase1, loadout2, "Elint");
        F16 barak = new F16(50, "ready", homeBase1, "Thermal", loadout1);
        Zik hermes450 = new Zik(105, "ready", homeBase2, homeBase2.GetCoordinate(), "Regular", "Thermal");
        AttackMission attackWeeb = new AttackMission(target1,"Mario", barak, "");
        BdaMission ensureWeebIsDead = new BdaMission(target1, "Robocop", hermes450);
        IntelligenceMission findSecondWeebHideout = new IntelligenceMission(target2, "Joe Mama",baz);
        attackWeeb.Begin();
        System.out.println(attackWeeb.ExecuteMission());
        attackWeeb.Finish();
        ensureWeebIsDead.Begin();
        ensureWeebIsDead.Cancel();
        ensureWeebIsDead.Begin();
        System.out.println(ensureWeebIsDead.ExecuteMission());
        ensureWeebIsDead.Finish();
        findSecondWeebHideout.Begin();
        System.out.println(findSecondWeebHideout.ExecuteMission());
        findSecondWeebHideout.Finish();
    }
}
