package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

public class Heron extends UAV{
    public Heron(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates) {
        super(hoursSinceRepair, flightStatus, 150, homeBase, currentCoordinates);
    }
}
