package AerialVehicles;


import Entities.Coordinates;
import Entities.Destination;

public abstract class AerialVehicle implements IAirVehicle {

    protected double hoursSinceRepair;



    protected Coordinates currentCoordinates;
    protected String flightStatus;
    protected final double MAX_FLIGHT_HOURS;

    // flightStatus can only be either "ready", "not ready" or "airborne"

    protected Destination homeBase;



    AerialVehicle(double hoursSinceRepair, String flightStatus, double maxFlightHours, Destination homeBase, Coordinates currentCoordinates){
        this.hoursSinceRepair = hoursSinceRepair;
        this.flightStatus = flightStatus;
        this.MAX_FLIGHT_HOURS = maxFlightHours;
        this.homeBase = homeBase;
        this.currentCoordinates = currentCoordinates;
    }
    public boolean isHome(){
        return (this.homeBase.GetCoordinate().equals(this.currentCoordinates));
    }
    public Destination getHomeBase() {
        return homeBase;
    }

    public void setHomeBase(Destination homeBase) {
        this.homeBase = homeBase;
    }

    public void repair(){
        this.hoursSinceRepair = 0;
        this.flightStatus = "ready";
    }
    public void check(){
        if(this.hoursSinceRepair >= this.MAX_FLIGHT_HOURS && this.isHome()){
            this.flightStatus = "not ready";
            this.repair();
        }
        else{
            this.flightStatus = "ready";
        }
    }
    public void flyTo(Destination destination){
        if (this.flightStatus.equals("not ready")){
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
        else{
            System.out.println("Flying to " + destination.GetObjectiveName());
            this.flightStatus = "airborne";
            setCurrentCoordinates(destination.GetCoordinate());
        }
        
    }
    public void land(Destination destination){
        System.out.println("Landing on " + destination.GetObjectiveName());
        this.check();
    }
    public double getHoursSinceRepair() {
        return hoursSinceRepair;
    }

    public void setHoursSinceRepair(double hoursSinceRepair) {
        this.hoursSinceRepair = hoursSinceRepair;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getCurrentCoordinates() {
        return currentCoordinates;
    }


    public void setCurrentCoordinates(Coordinates currentCoordinates) {
        this.currentCoordinates = currentCoordinates;
    }
}
