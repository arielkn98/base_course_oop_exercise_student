package AerialVehicles;


import Entities.Coordinates;
import Entities.Destination;

public class Zik extends Hermes implements IIntelligenceAircraft, IBDAAir{
    private String camera;
    private String sensor;

    public Zik(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates, String camera, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.camera = camera;
        this.sensor = sensor;
    }

    @Override
    public String getCamera() {
        return this.camera;
    }

    @Override
    public void setCamera(String camera) {
        this.camera = camera;
    }

    @Override
    public String getSensor() {
        return this.sensor;
    }

    @Override
    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}