package AerialVehicles;


import Entities.Coordinates;
import Entities.Destination;

import java.util.*;

public class F15 extends FighterJet implements IFighter, IIntelligenceAircraft{

    private List<String> missiles;
    private String sensor;

    public F15(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates, List<String> missiles, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.missiles = missiles;
        this.sensor = sensor;
    }

    public F15(double hoursSinceRepair, String flightStatus, Destination homeBase, List<String> missiles, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase);
        this.missiles = missiles;
        this.sensor = sensor;
    }

    @Override
    public List<String> getMissiles() {
        return this.missiles;
    }

    @Override
    public void setMissiles(List<String> missiles) {
        this.missiles = missiles;
    }

    @Override
    public int getNumMissiles() {
        return this.missiles.size();
    }

    @Override
    public String getSensor() {
        return this.sensor;
    }

    @Override
    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}
