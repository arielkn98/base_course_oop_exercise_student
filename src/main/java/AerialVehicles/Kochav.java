package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

import java.util.List;

public class Kochav extends Hermes implements IFighter, IIntelligenceAircraft, IBDAAir{

    private String camera;
    private List<String> missiles;
    private String sensor;

    public Kochav(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates, String camera, List<String> missiles, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.camera = camera;
        this.missiles = missiles;
        this.sensor = sensor;
    }

    @Override
    public String getCamera() {
        return this.camera;
    }

    @Override
    public void setCamera(String camera) {
        this.camera = camera;
    }

    @Override
    public List<String> getMissiles() {
        return this.missiles;
    }

    @Override
    public int getNumMissiles() {
        return this.missiles.size();
    }

    @Override
    public void setMissiles(List<String> missiles) {
        this.missiles = missiles;
    }

    @Override
    public String getSensor() {
        return this.sensor;
    }

    @Override
    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}
