package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

import java.util.List;

public class Eitan extends Heron implements IFighter,IIntelligenceAircraft {

    private List<String> missiles;
    private String sensor;

    public Eitan(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates, List<String> missiles, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.missiles = missiles;
        this.sensor = sensor;
    }

    @Override
    public List<String> getMissiles() {
        return this.missiles;
    }

    @Override
    public int getNumMissiles() {
        return this.missiles.size();
    }

    @Override
    public void setMissiles(List<String> missiles) {
        this.missiles = missiles;
    }

    @Override
    public String getSensor() {
        return this.sensor;
    }

    @Override
    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}
