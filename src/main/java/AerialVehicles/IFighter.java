package AerialVehicles;

import java.util.*;

public interface IFighter  extends IAirVehicle {
    // interface for aircraft that can perform attack missions
    List<String> getMissiles();
    // Missiles can include "Python", "AMRAAM" and "Spice250"
    int getNumMissiles();
    void setMissiles(List<String> missiles);
}
