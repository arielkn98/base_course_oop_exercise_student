package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

public interface IAirVehicle {
    // linking interface that insures that any AerialVehicle instance can be a class
    // that just implements either IFighter, IBDAAir or IIntelligenceAir
    void repair();
    void check();
    Destination getHomeBase();
    void setHomeBase(Destination homeBase);
    void flyTo(Destination destination);
    void land(Destination destination);
    double getHoursSinceRepair();
    void setHoursSinceRepair(double hoursSinceRepair);
    Coordinates getCurrentCoordinates();
    void setCurrentCoordinates(Coordinates currentCoordinates);
}
