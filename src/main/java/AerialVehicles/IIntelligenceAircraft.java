package AerialVehicles;

public interface IIntelligenceAircraft  extends IAirVehicle {
    // interface for aircraft that can perform intelligence missions
    // Can have one of two sensors: "InfraRed" and "Elint"
    String getSensor();
    void setSensor(String sensor);
}
