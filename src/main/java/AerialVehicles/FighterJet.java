package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

public abstract class FighterJet extends AerialVehicle{
    FighterJet(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates) {
        super(hoursSinceRepair, flightStatus, 250, homeBase, currentCoordinates);
    }
    FighterJet(double hoursSinceRepair, String flightStatus, Destination homeBase) {
        super(hoursSinceRepair, flightStatus, 250, homeBase, homeBase.GetCoordinate());
    }
}
