package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

public class Hermes extends UAV{
    public Hermes(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates) {
        super(hoursSinceRepair, flightStatus, 150, homeBase, currentCoordinates);
    }
}
