package AerialVehicles;

public interface IBDAAir  extends IAirVehicle {
    // interface for aircraft that can perform BDA missions
    // can have one of four camera types: "Regular", "Thermal" and "NightVision"
    String getCamera();
    void setCamera(String camera);
}
