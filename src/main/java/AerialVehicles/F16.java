package AerialVehicles;


import Entities.Coordinates;
import Entities.Destination;

import java.util.List;

public class F16 extends FighterJet implements IFighter, IBDAAir{

    private String camera;
    private List<String> missiles;

    public F16(double hoursSinceRepair, String flightStatus, Destination homeBase, Coordinates currentCoordinates, String camera, List<String> missiles) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.camera = camera;
        this.missiles = missiles;
    }

    public F16(double hoursSinceRepair, String flightStatus, Destination homeBase, String camera, List<String> missiles) {
        super(hoursSinceRepair, flightStatus, homeBase);
        this.camera = camera;
        this.missiles = missiles;
    }

    @Override
    public String getCamera() {
        return this.camera;
    }

    @Override
    public void setCamera(String camera) {
        this.camera = camera;
    }

    @Override
    public List<String> getMissiles() {
        return this.missiles;
    }

    @Override
    public int getNumMissiles() {
        return this.missiles.size();
    }


    @Override
    public void setMissiles(List<String> missiles) {
        this.missiles = missiles;
    }
}
