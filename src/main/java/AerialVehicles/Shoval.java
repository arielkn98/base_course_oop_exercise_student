package AerialVehicles;


import Entities.Coordinates;
import Entities.Destination;

import java.util.List;

public class Shoval extends Heron implements IFighter, IBDAAir, IIntelligenceAircraft{

    private List<String> missiles;
    private String camera;
    private String sensor;

    public Shoval(double hoursSinceRepair, String flightStatus, double maxFlightHours, Destination homeBase, Coordinates currentCoordinates, List<String> missiles, String camera, String sensor) {
        super(hoursSinceRepair, flightStatus, homeBase, currentCoordinates);
        this.missiles = missiles;
        this.camera = camera;
        this.sensor = sensor;
    }

    @Override
    public String getCamera() {
        return this.camera;
    }

    @Override
    public void setCamera(String camera) {
        this.camera = camera;
    }

    @Override
    public List<String> getMissiles() {
        return this.missiles;
    }

    @Override
    public int getNumMissiles() {
        return this.missiles.size();
    }

    @Override
    public void setMissiles(List<String> missiles) {
        this.missiles = missiles;
    }

    @Override
    public String getSensor() {
        return null;
    }

    @Override
    public void setSensor(String sensor) {
        this.sensor = sensor;
    }
}

