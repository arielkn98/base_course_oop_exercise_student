package AerialVehicles;

import Entities.Coordinates;
import Entities.Destination;

public abstract class UAV extends AerialVehicle{
    public UAV(double hoursSinceRepair, String flightStatus, double maxFlightHours, Destination homeBase, Coordinates currentCoordinates) {
        super(hoursSinceRepair, flightStatus, maxFlightHours, homeBase, currentCoordinates);
    }

    public String hoverOverLocation(Destination destination){
        this.flightStatus = "airborne";
        double latitude = destination.GetCoordinate().getLatitude();
        double longitude = destination.GetCoordinate().getLongitude();
        String message = String.format("Hovering Over: %f, %f", latitude, longitude);
        System.out.println(message);
        return message;
    }
}
