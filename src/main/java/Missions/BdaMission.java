package Missions;

import AerialVehicles.IBDAAir;
import Entities.Destination;

public class BdaMission implements IMission{

    private Destination destination;
    private String pilotName;
    private IBDAAir aerialVehicle;
    private String message;

    public BdaMission(Destination destination, String pilotName, IBDAAir aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
        this.message = "";
    }

    public String getTarget() {
        return this.destination.GetObjectiveName();
    }

    public void setTarget(String target) {
        this.destination.SetObjectiveName(target);
    }

    @Override
    public String ExecuteMission() {
        String aircraftName = this.aerialVehicle.getClass().getName();
        String objectiveName = this.destination.GetObjectiveName();
        String camera = this.aerialVehicle.getCamera();
        this.message = String.format("%s: %s taking pictures of %s with: %s", this.pilotName, aircraftName,
                objectiveName, camera);
        return this.message;
    }

    @Override
    public void Begin(){
        System.out.println("Beginning Mission!");
        this.aerialVehicle.flyTo(destination);
    }

    @Override
    public void Cancel(){
        System.out.println("Abort mission!");
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
    }

    @Override
    public void Finish(){
        ExecuteMission();
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
        System.out.println("Finish Mission!");
    }

    @Override
    public void FlyAircraft(double numHours){
        this.aerialVehicle.setHoursSinceRepair(this.aerialVehicle.getHoursSinceRepair()+numHours);
    }

    @Override
    public Destination getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @Override
    public String getPilotName() {
        return this.pilotName;
    }

    @Override
    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public IBDAAir getAerialVehicle() {
        return this.aerialVehicle;
    }

    public void setAerialVehicle(IBDAAir aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
