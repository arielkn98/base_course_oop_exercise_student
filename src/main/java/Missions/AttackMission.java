package Missions;

import AerialVehicles.IFighter;
import Entities.Destination;

import java.util.*;

public class AttackMission implements IMission{

    private Destination destination;
    private String pilotName;
    private IFighter aerialVehicle;
    private String message;


    public AttackMission(Destination destination, String pilotName, IFighter aerialVehicle, String message) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
        this.message = message;
    }

    public String getTarget() {
        return this.destination.GetObjectiveName();
    }

    public void setTarget(String target) {
        this.destination.SetObjectiveName(target);
    }

    public boolean canAttack(){
        return (this.aerialVehicle.getMissiles().size() > 0);
    }
    private String useMissiles(){
        String most_common_missile = mostCommonMissile();
        List<String> missiles = new ArrayList<String>(this.aerialVehicle.getMissiles());
        missiles.removeAll(Collections.singleton(most_common_missile));
        this.aerialVehicle.setMissiles(missiles);
        return most_common_missile;
    }
    private int numOfMissileType(String missileType){
        List<String> list = this.aerialVehicle.getMissiles();
        return Collections.frequency(list, missileType);
    }

    //function that checks for most common missile type
    private String mostCommonMissile(){
        return mostItem(this.aerialVehicle.getMissiles());
    }
    // helper function:
    private <String> String mostItem(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        for (String t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<String, Integer> max = null;

        for (Map.Entry<String, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue())
                max = e;
        }

        return max.getKey();
    }

    @Override
    public String ExecuteMission(){
        // function checks if there is sufficient ammo, and if there is- attack with
        // all of the missiles of the most available type
        String aircraftName = this.aerialVehicle.getClass().getName();
        if(canAttack()) {
            String missileType = mostCommonMissile();
            int numMissilesOfType = numOfMissileType(missileType);
            setMessage(String.format("%s: %s attacking %s with %sX%d",this.pilotName, aircraftName,
                    this.destination.GetObjectiveName(),missileType,numMissilesOfType));
            useMissiles();
        }
        else{
            setMessage(String.format("%s unable to attack %s", aircraftName, this.destination.GetObjectiveName()));
        }
        return this.message;
    }
    public void Begin(){
        System.out.println("Beginning Mission!");
        this.aerialVehicle.flyTo(destination);
    }

    public void Cancel(){
        System.out.println("Abort mission!");
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
    }

    public void Finish(){
        ExecuteMission();
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
        System.out.println("Finish Mission!");
    }

    // Make a function to simulate missions of selected length by user's choice
    public void FlyAircraft(double numHours){
        this.aerialVehicle.setHoursSinceRepair(this.aerialVehicle.getHoursSinceRepair()+numHours);
    }
    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getPilotName() {
        return this.pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public IFighter getAerialVehicle() {
        return aerialVehicle;
    }

    public void setAerialVehicle(IFighter aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
