package Missions;

import AerialVehicles.IAirVehicle;
import Entities.Destination;

public interface IMission {
    String ExecuteMission();
    void Begin();
    void Cancel();
    void Finish();
    void FlyAircraft(double numHours);
    Destination getDestination();
    void setDestination(Destination destination);
    String getPilotName();
    void setPilotName(String pilotName);
//    IAirVehicle getAerialVehicle();
//    void setAerialVehicle(IAirVehicle aerialVehicle);
    String getMessage();
    void setMessage(String message);
}
