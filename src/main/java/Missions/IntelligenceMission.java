package Missions;

import AerialVehicles.IIntelligenceAircraft;
import Entities.Destination;

public class IntelligenceMission implements IMission{

    private Destination destination;
    private String pilotName;
    private IIntelligenceAircraft aerialVehicle;
    private String message;

    public IntelligenceMission(Destination destination, String pilotName, IIntelligenceAircraft aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
        this.message = "";
    }

    @Override
    public String ExecuteMission() {
        String aircraftName = this.aerialVehicle.getClass().getName();
        String objectiveName = this.destination.GetObjectiveName();
        String sensor = this.aerialVehicle.getSensor();
        this.message = String.format("%s: %s collecting data of %s with: %s", this.pilotName, aircraftName,
                objectiveName, sensor);
        return this.message;
    }

    @Override
    public void Begin(){
        System.out.println("Beginning Mission!");
        this.aerialVehicle.flyTo(destination);
    }

    @Override
    public void Cancel(){
        System.out.println("Abort mission!");
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
    }

    @Override
    public void Finish(){
        ExecuteMission();
        this.aerialVehicle.land(this.aerialVehicle.getHomeBase());
        System.out.println("Finish Mission!");
    }

    @Override
    public void FlyAircraft(double numHours){
        this.aerialVehicle.setHoursSinceRepair(this.aerialVehicle.getHoursSinceRepair()+numHours);
    }

    @Override
    public Destination getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @Override
    public String getPilotName() {
        return this.pilotName;
    }

    @Override
    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public IIntelligenceAircraft getAerialVehicle() {
        return this.aerialVehicle;
    }

    public void setAerialVehicle(IIntelligenceAircraft aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
