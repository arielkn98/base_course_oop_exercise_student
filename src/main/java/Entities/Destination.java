package Entities;

public class Destination{

    private Coordinates coordinate;
    private String objectiveName1;

    public Destination(Coordinates coordinate, String objectiveName){
        this.coordinate = coordinate;
        this.objectiveName1 = objectiveName;
    }

    public Coordinates GetCoordinate(){
        return this.coordinate;
    }
    public String GetObjectiveName(){
        return this.objectiveName1;
    }
    public void SetCoordinate(Coordinates coordinate){
        this.coordinate = coordinate;
    }
    public void SetObjectiveName(String objectiveName){
        this.objectiveName1 = objectiveName;
    }
    
}
